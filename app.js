function suma() {
    ejecutarOperacion('suma');
}

function resta() {
    ejecutarOperacion('resta');
}

function multiplica() {
    ejecutarOperacion('multiplica');
}

function divide() {
    ejecutarOperacion('divide');
}

function ejecutarOperacion(operacion) {
    let num1 = document.getElementById('numberOne').value;
    let num2 = document.getElementById('numberTwo').value;
    num1 = parseInt(num1);
    num2 = parseInt(num2);

    if(num1 == "" || num2 == "" || isNaN(num1) || isNaN(num2)) {
        alert("Por favor, rellena las dos cajas de texto. Introduce sólo números");
    } else {
        let resultado = 0;
        const operationSymbol = document.getElementById('operation');
        switch (operacion) {
            case 'suma':
                resultado = num1 + num2;
                operationSymbol.textContent = '+';
                break;
            case 'resta':
                resultado = num1 - num2;
                operationSymbol.textContent = '-';
                break;
            case 'multiplica':
                resultado = num1 * num2;
                operationSymbol.textContent = 'x';
                break;
            case 'divide':
                resultado = num1 / num2;
                operationSymbol.textContent = '/';
                break;
        }
        document.getElementById('resultado').value = resultado;
    } 

}